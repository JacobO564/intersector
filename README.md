## Getting started with intersector

Run :

```
python3 -m pip install .
```

from the cloned directory. Here is what you can do to get to your function:

```
>>> import intersector
>>> dir(intersector)
['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'intersect']
>>> intersector.intersect()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: intersect() missing 1 required positional argument: 'polygon'
>>>
```

## TODOs

This should be part of your todo list till Tuesday before class.

- [ ] Implement an empty intersect function
- [ ] First write your tests so that you can execute them using pytest
- [ ] Then implement your function. Pass all your tests.
- [ ] Make sure your coverage is 100% (pylint is 10/10)
- [ ] Optimize your function to be as fast as possible.
