import sys
import glob
from time import time
from pathlib import Path
sys.path.append("..")
from intersector.intersector import intersect
import json
from numpy.testing import assert_almost_equal
from pprint import pprint

def interval_equality(l1, l2):
    """
    takes two sets of intervals and computes their distance
    sample
    l1 = [ ((0,0), (1,0)) ] 
    l2 = [ ((0.001,0), (1,0)) ] 
    """
    assert len(l1) == len(l2), "FATAL: The number of elements in the outputs Mismatch"
    for i,j in zip(l1,l2):
        p1,p2 = i
        q1,q2 = j
        assert_almost_equal(p1,q1)
        assert_almost_equal(p2,q2)

def test_square_1():
    unit_square = [(0, 0), (1, 0), (1, 1), (0, 1)]
    output = intersect(unit_square, a_val=1, b_val=0, c_val=0)
    assert len(output) == 1


def test_jsons():
    FDIR = Path(__file__).resolve().parent
    tot_time = 0
    tot_count = 0
    for testcase in glob.glob(str(FDIR)+"/**/*.json"):
        tot_count+=1
        print("Processing:", testcase)
        tdata = open(testcase).read()
        data = json.loads(tdata)
        # Feed the input from data to intersect function and check its output
        print(data)
        output = data['output']
        data.pop('output', None)
        start_time = time()
        coutput = intersect(**data)
        tot_time += (time() - start_time)
        print("Testing Approximate Equality:")
        pprint(output)
        pprint(coutput)
        print("="*40)
        interval_equality(output,coutput)
    print("Total time taken:", tot_time)
    print("Total Files checked:", tot_count)

